package models

import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._

class PhoneNumber {
  var id: Long = 0
  var number: String = ""
  var userid: Long = 0
}
