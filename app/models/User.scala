package models

import play.api.data._
import play.api.data.validation.Constraints._

class User {
    var id: Long = 0
    var name: String = ""
    var password: String = ""
    var email: String = ""
    var phone: List[PhoneNumber] = _
}

