package models

import play.api.data.Form
import play.api.data.Forms._

case class UserEditForm (
                      email: String,
                      phone1: String,
                      phone2: String
                    )

object UserEditForm {
  val form = Form(mapping(
    "email" -> email,
    "phone1" -> text,
    "phone2" -> text
  )
  (UserEditForm.apply)(UserEditForm.unapply))
}