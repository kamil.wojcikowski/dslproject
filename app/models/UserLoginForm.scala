package models

import play.api.data.Form
import play.api.data.Forms._

case class UserLoginForm(name : String, password : String)

object UserLoginForm {
  val form = Form(mapping(
    "name" -> nonEmptyText(),
    "password" -> text
  )
  (UserLoginForm.apply)(UserLoginForm.unapply))
}
