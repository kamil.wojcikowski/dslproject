package models

import play.api.data.Form
import play.api.data.Forms._

case class UserForm (
                    name: String,
                    password: String,
                    email: String,
                    phone1: String,
                    phone2: String
                    )

object UserForm {
  val form = Form(mapping(
    "name" -> nonEmptyText.verifying("User exists", name => !DB.userExists(name)),
    "password" -> nonEmptyText.verifying("5 znakow w tym 1 specjalny i 1 wielka litera",
      pass => pass.matches("^(?=.*[A-Z])(?=.*[$@$!%*?&^])[A-Za-z\\d$@$!%^*?&]{5,}")),
    "email" -> email,
    "phone1" -> text,
    "phone2" -> text
  )
  (UserForm.apply)(UserForm.unapply))
}