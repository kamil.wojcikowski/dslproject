package models

import anorm._
import java.sql.{Connection, DriverManager}
import org.mindrot.jbcrypt._

object DB {

  Class.forName("org.h2.Driver")
  implicit val connection: Connection = getConnection("jdbc:h2:mem:play")

  def getConnection(url: String): Connection = {
    DriverManager.getConnection(url,"sa","")
  }

  def dropAndCreate() {
    create()
  }

  def drop() {
    SQL("""
        DROP TABLE IF EXISTS User;
        DROP TABLE IF EXISTS phonenumber;
      """).executeUpdate()
  }

  def create() {
    SQL("""
        CREATE TABLE IF NOT EXISTS User (
           id bigint(20) NOT NULL,
           name varchar(255) NOT NULL,
           email varchar(255) NOT NULL,
           password varchar(255) NOT NULL,
           PRIMARY KEY (id)
        );
        CREATE TABLE IF NOT EXISTS phonenumber (
            id bigint(20) NOT NULL,
            number varchar(255) NOT NULL,
            userid bigint(20) NOT NULL,
            PRIMARY KEY (id)
         );
      """).executeUpdate()
  }



  def insertUser(user: User): Unit = {
    user.id = getNextUserId()
    SQL("insert into User(id, name, email, password) values ({id}, {name}, {email}, {password})")
      .on("id" -> user.id, "name" -> user.name, "email" -> user.email, "password" -> BCrypt.hashpw(user.password, BCrypt.gensalt)).executeInsert()
    for (PhoneNumber <- user.phone) {
      SQL("insert into phonenumber(id, number, userid) values ({id}, {number}, {userid})")
        .on("id" -> getNextPhoneId(), "number" -> PhoneNumber.number, "userid" -> user.id).executeInsert()
    }
  }

  def selectUser(userid: Long): User = {
    val statement = connection.createStatement()
    val phoneNumbers = statement.executeQuery("SELECT * from phonenumber WHERE userid = '" + userid + "'")
    val numberBuilder = List.newBuilder[PhoneNumber]

    while(phoneNumbers.next()) {
      val id = phoneNumbers.getLong("id")
      val number = phoneNumbers.getString("number")
      val userId = phoneNumbers.getLong("userId")
      val phonenumber = new PhoneNumber
      phonenumber.id = id
      phonenumber.number = number
      phonenumber.userid = userId
      numberBuilder += phonenumber
    }

    val numberList = numberBuilder.result()

    val user = statement.executeQuery("SELECT * from User WHERE id = " + userid)

    if(user.next()) {
      val selectedUser = new User
      selectedUser.name = user.getString("name")
      selectedUser.email = user.getString("email")
      selectedUser.password = user.getString("password")
      selectedUser.phone = numberList
      return selectedUser
    } else {
      return null
    }
  }

  def getNextUserId(): Long = {
    val statement = connection.createStatement()
    val result = statement.executeQuery("SELECT MAX(id) as id from User")
    if(result.next()) {
      val maxId = result.getLong("id")
      return maxId+1
    } else {
      return 1
    }
  }

  def getNextPhoneId(): Long = {
    val statement = connection.createStatement()
    val result = statement.executeQuery("SELECT MAX(id) as id from phonenumber")
    if(result.next()) {
      val maxId = result.getLong("id")
      return maxId+1
    } else {
      return 1
    }
  }

  def updateUser(userId: Long, newUser: User): Unit = {
    for(PhoneNumber <- newUser.phone) {
      SQL("UPDATE phonenumber SET number = {number} WHERE userId = {userid} AND id = {phoneid}")
        .on("phoneid" -> PhoneNumber.id, "number" -> PhoneNumber.number, "userid" -> userId).executeUpdate()
    }
    if(newUser.email != null) {
      SQL("UPDATE User SET  email = {email} WHERE id = {id}")
        .on("id" -> userId, "email" -> newUser.email).executeUpdate()
    }
  }

  def userExists(name: String): Boolean = {
    val statement = connection.createStatement()
    val result = statement.executeQuery("SELECT name from User WHERE name = '" + name + "'")
    return result.next()
  }

  def findUserByName(name: String): User = {
    val statement = connection.createStatement()

    val user = statement.executeQuery("SELECT * from User WHERE name = '" + name +"'")

    val selectedUser = new User
    if(user.next()) {
      selectedUser.id = user.getLong("id")
      selectedUser.name = user.getString("name")
      selectedUser.email = user.getString("email")
      selectedUser.password = user.getString("password")
    } else {
      return null
    }

    val phoneNumbers = statement.executeQuery("SELECT * from phonenumber WHERE userid = '" + selectedUser.id + "'")
    val numberBuilder = List.newBuilder[PhoneNumber]

    while(phoneNumbers.next()) {
      val id = phoneNumbers.getLong("id")
      val number = phoneNumbers.getString("number")
      val userId = phoneNumbers.getLong("userId")
      val phonenumber = new PhoneNumber
      phonenumber.id = id
      phonenumber.number = number
      phonenumber.userid = userId
      numberBuilder += phonenumber
    }
    selectedUser.phone = numberBuilder.result()
    return selectedUser
  }

  def getAllUsers(): List[User] = {
    val statement = connection.createStatement()
    val results = statement.executeQuery("SELECT * from User")

    val userBuilder = List.newBuilder[User]

    while(results.next()) {
      val user = new User
      user.name = results.getString("name")
      user.email = results.getString("email")
      user.password = results.getString("password")
      userBuilder += user
    }
    return userBuilder.result()
  }

}