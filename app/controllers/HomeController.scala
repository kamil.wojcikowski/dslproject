package controllers

import javax.inject._
import models._
import play.api._
import play.api.mvc._
import org.mindrot.jbcrypt._
import play.api.i18n.{I18nSupport, MessagesApi}

@Singleton
class HomeController @Inject()(cc: ControllerComponents, messagesApi: MessagesApi)
    extends AbstractController(cc) with I18nSupport {

  def index() = Action { implicit request: Request[AnyContent] =>
    DB.dropAndCreate()
    val userList = DB.getAllUsers()
    Ok(views.html.index(request, userList))
  }

  def register: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.register(UserForm.form))
  }

  def submitRegister: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    UserForm.form.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.register(formWithErrors))
      },
      formData => {
        if (DB.userExists(formData.name)) {
          Redirect("/")
        } else {
          val user = new User
          user.name = formData.name
          user.password = formData.password
          user.email = formData.email
          val firstNumber = new PhoneNumber()
          firstNumber.number = formData.phone1
          val secondNumber = new PhoneNumber()
          secondNumber.number = formData.phone2
          user.phone = List(firstNumber, secondNumber)

          DB.insertUser(user)
          Redirect("/").withSession("logged_user_session" -> formData.name)
        }
      }
    )
  }

  def logout: Action[AnyContent] = Action {
    Redirect("/").withSession("logged_user_session" -> "")
  }

  def login: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.login(UserLoginForm.form))
  }

  def submitLogin: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    UserLoginForm.form.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.login(formWithErrors))
      },
      formData => {
        val name = formData.name
        val password = formData.password
        val DBUser = DB.findUserByName(name)
        if (DBUser != null && BCrypt.checkpw(password.toString, DBUser.password.toString)) {
          Redirect("/").withSession("logged_user_session" -> name)
        } else {
          Redirect("/")
        }
      }
    )
  }

  def details(name: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val user = DB.findUserByName(name)
    Ok(views.html.details(user, user.phone, UserEditForm.form))
  }

  def edit(name: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val DBuser = DB.findUserByName(name)
    UserEditForm.form.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.details(DBuser, DBuser.phone, formWithErrors))
      },
      formData => {

        if(formData.email != null) {
          DBuser.email = formData.email
        }
          val firstNumber = DBuser.phone(0)
          firstNumber.number = formData.phone1

          val secondNumber = DBuser.phone(1)
          secondNumber.number = formData.phone2

          DBuser.phone = List(firstNumber, secondNumber)

          DB.updateUser(DBuser.id, DBuser)
          Redirect("/").withSession("logged_user_session" -> name)
        }
      )
  }







}
